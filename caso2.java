class Empleado {
    private String nombre;
    private String apellidos;	
    private String numeroCedula;
    private String direccion;
    private int anosAntiguedad;
    private int telefono;
    private Double salrio;
    private String jefeinmediato;
        //Public String Apellidos;

	public Empleado(){
            

	}

    public Empleado(String nombre, String apellidos, String numeroCedula, String direccion, int anosAntiguedad, int telefono, Double salrio, String jefeinmediato) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.numeroCedula = numeroCedula;
        this.direccion = direccion;
        this.anosAntiguedad = anosAntiguedad;
        this.telefono = telefono;
        this.salrio = salrio;
        this.jefeinmediato = jefeinmediato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNumeroCedula() {
        return numeroCedula;
    }

    public void setNumeroCedula(String numeroCedula) {
        this.numeroCedula = numeroCedula;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getAnosAntiguedad() {
        return anosAntiguedad;
    }

    public void setAnosAntiguedad(int anosAntiguedad) {
        this.anosAntiguedad = anosAntiguedad;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public Double getSalrio() {
        return salrio;
    }

    public void setSalrio(Double salrio) {
        this.salrio = salrio;
    }

    public String getJefeinmediato() {
        return jefeinmediato;
    }

    public void setJefeinmediato(String jefeinmediato) {
        this.jefeinmediato = jefeinmediato;
    }

    @Override
    public String toString() {
        return "Empleado{" + "nombre=" + nombre + ", apellidos=" + apellidos + ", numeroCedula=" + numeroCedula + ", direccion=" + direccion + ", anosAntiguedad=" + anosAntiguedad + ", telefono=" + telefono + ", salrio=" + salrio + ", jefeinmediato=" + jefeinmediato + '}';
        
    }
   
    
}
class Secretario extends Empleado {

	    private String despacho;
        private int numerofax;
        private int porcentajeIncremento;

    public Secretario(String despacho, int numerofax, int porcentajeIncremento) {
        this.despacho = despacho;
        this.numerofax = numerofax;
        this.porcentajeIncremento = porcentajeIncremento;
    }

    public Secretario(String despacho, int numerofax, int porcentajeIncremento, String nombre, String apellidos, String numeroCedula, String direccion, int anosAntiguedad, int telefono, Double salrio, String jefeinmediato) {
        super(nombre, apellidos, numeroCedula, direccion, anosAntiguedad, telefono, salrio, jefeinmediato);
        this.despacho = despacho;
        this.numerofax = numerofax;
        this.porcentajeIncremento = porcentajeIncremento;
    }

    public String getDespacho() {
        return despacho;
    }

    public void setDespacho(String despacho) {
        this.despacho = despacho;
    }

    public int getNumerofax() {
        return numerofax;
    }

    public void setNumerofax(int numerofax) {
        this.numerofax = numerofax;
    }

    public int getPorcentajeIncremento() {
        return porcentajeIncremento;
    }

    public void setPorcentajeIncremento(int porcentajeIncremento) {
        this.porcentajeIncremento = porcentajeIncremento;
    }
	
	public Double getAumentarSalrio() {
        Double salario = getSalrio(); 
        return salario = salario + (salario * porcentajeIncremento) / 100 ;
    }

    @Override
    public String toString() {
        return super.toString() +  "Secretario{" + "despacho=" + despacho + ", numerofax=" + numerofax + ", porcentajeIncremento=" + porcentajeIncremento + '}';
        //return "Secretario{" + "despacho=" + despacho + ", numerofax=" + numerofax + ", porcentajeIncremento=" + porcentajeIncremento + '}';
    }
}
    
class Vendedor extends Empleado{

		private String marca;
        private String modelo;
        private String placa;
        private String celular;
        private String departamento;
        private String listaclientes;
        private int porcntajeincrementoventa;

    
    public Vendedor(String marca, String modelo, String placa, String celular, String departamento, String listaclientes, int porcntajeincrementoventa, String nombre, String apellidos, String numeroCedula, String direccion, int anosAntiguedad, int telefono, Double salrio, String jefeinmediato) {
        super(nombre, apellidos, numeroCedula, direccion, anosAntiguedad, telefono, salrio, jefeinmediato);
        this.marca = marca;
        this.modelo = modelo;
        this.placa = placa;
        this.celular = celular;
        this.departamento = departamento;
        this.listaclientes = listaclientes;
        this.porcntajeincrementoventa = porcntajeincrementoventa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getListaclientes() {
        return listaclientes;
    }

    public void setListaclientes(String listaclientes) {
        this.listaclientes = listaclientes;
    }

    public int getPorcntajeincrementoventa() {
        return porcntajeincrementoventa;
    }

    public void setPorcntajeincrementoventa(int porcntajeincrementoventa) {
        this.porcntajeincrementoventa = porcntajeincrementoventa;
    }
	
	public Double getAumentarSalrio() {
        Double salario = getSalrio(); 
        return salario = salario + (salario * porcntajeincrementoventa) / 100 ;
    }

    @Override
    public String toString() {
        return super.toString() +  "Vendedor{" + "marca=" + marca + ", modelo=" + modelo + ", placa=" + placa + ", celular=" + celular + ", departamento=" + departamento + ", listaclientes=" + listaclientes + ", porcntajeincrementoventa=" + porcntajeincrementoventa + '}';
    }
        
}
class JefeZona extends Empleado{
        private String Despacho;
		private String ListaVendedores;
        private String Placa;
        private String Modelo;
        private String Marca;

    public JefeZona(String Despacho, String ListaVendedores, String Placa, String Modelo, String Marca, String nombre, String apellidos, String numeroCedula, String direccion, int anosAntiguedad, int telefono, Double salrio, String jefeinmediato) {
        super(nombre, apellidos, numeroCedula, direccion, anosAntiguedad, telefono, salrio, jefeinmediato);
        this.Despacho = Despacho;
        this.ListaVendedores = ListaVendedores;
        this.Placa = Placa;
        this.Modelo = Modelo;
        this.Marca = Marca;
    }

    public String getDespacho() {
        return Despacho;
    }

    public void setDespacho(String Despacho) {
        this.Despacho = Despacho;
    }

    public String getListaVendedores() {
        return ListaVendedores;
    }

    public void setListaVendedores(String ListaVendedores) {
        this.ListaVendedores = ListaVendedores;
    }

    public String getPlaca() {
        return Placa;
    }

    public void setPlaca(String Placa) {
        this.Placa = Placa;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }
	
	public Double getAumentarSalrio() {
        Double salario = getSalrio(); 
        return salario = salario + (salario * 20) / 100 ;
    }
	
	
    @Override
    public String toString() {
        return super.toString() +  "JefeZona{" + "Despacho=" + Despacho + ", ListaVendedores=" + ListaVendedores + ", Placa=" + Placa + ", Modelo=" + Modelo + ", Marca=" + Marca + '}';
    }


}
 

public class caso2{
    public static void main(String[] args){
	   Secretario datosSecretario = new Secretario("Despacho",628085,5, "Santiago", "Abad", "0487584575", "Santa Rita",5,1,400.00,"Carlos Luna");    
	   String datosSec = datosSecretario.toString();
       System.out.println("Secretario");
	   System.out.println(datosSec);
	   System.out.println("Sueldo más incremento");
	   System.out.println(datosSecretario.getAumentarSalrio());
	   
	   System.out.println("**************");
	   Vendedor datosVendedor = new Vendedor("Honda", "Civic", "PKC-2584", "097845612", "Departamento norte", "Clientes", 10, "Andres", "Cevallos", "0445845575", "Solanda",5,1,400.00,"Carlos Luna");    
	   String datosVen = datosVendedor.toString();
       System.out.println("Vendedor");
	   System.out.println(datosVen);
	   System.out.println("Sueldo más incremento");
	   System.out.println(datosVendedor.getAumentarSalrio());
	   
	   System.out.println("**************");
	   JefeZona datosJefeZona = new JefeZona("Despacho","ListaVendedores","PGH-25485","Civic", "Honda", "Andres", "Cevallos", "0445845575", "Solanda",5,1,400.00,"Carlos Luna");    
	   String datosJefe = datosJefeZona.toString();
       System.out.println("jefe de Zona");
	   System.out.println(datosJefe);
	   System.out.println("Sueldo más incremento del 20 %");
	   System.out.println(datosJefeZona.getAumentarSalrio());
	}
	      
   
}


